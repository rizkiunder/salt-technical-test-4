<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePrepaidBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->integer('p_id')->autoIncrement();
            $table->string('p_name')->default(0);
            $table->text('p_address')->nullable();
            $table->integer('p_price')->default(0);
            $table->string('p_mobile_phone')->default(0);
            $table->integer('p_value')->default(0);
            $table->integer('p_type')->default(0);
            $table->integer('p_user')->default(0);
            $table->string('p_shipping_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
