<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
    /* Style to change separator  */
    .breadcrumb-item + .breadcrumb-item::before {
        content: "|";
    }
</style>
</head>
<body class="bg-white">
    <div id="app">
        @auth
        <nav class="navbar navbar-expand-md navbar-light bg-light shadow-sm" style="--bs-breadcrumb-divider: '*';" >
            <div class="container">
                <div class="p-1">
                    <b>Hello, {{ Auth::user()->name }}</b><br>
                    <span class="text-danger">{{ session('unpaid') }}</span><span class="text-muted"> unpaid order</span>
                </div>
                <ol class="breadcrumb bg-light">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Prepaid Balance</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('product') }}">Product Page</a></li>
                </ol>
            </div>
        </nav>
        @endauth
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
