@extends('layouts.app')
@section('content')
<h2 for="email" class="col-md-4 text-md-right">{{ __('Pay your order') }}</h2>
<form method="POST" action="{{ route('payment_post') }}">
    @csrf
    @if($errors->has('message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <strong>Failed!</strong> {{ $errors->first('message') }}.
        </div>
    @endif
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="order_id" type="text" class="form-control @error('order_id') is-invalid @enderror" name="order_id" value="{{ $order_id }}" required autocomplete="order_id" autofocus placeholder="Order no.">
            @error('order_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center fixed-bottom">
        <div class="col-md-6 col-sm-11">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
        </div>
    </div>
</form>
@endsection
