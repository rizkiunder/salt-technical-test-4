@extends('layouts.app')
@section('content')
<h2 for="email" class="col-md-4 text-md-right">{{ __('Prepaid Balance') }}</h2>
<form method="POST" action="{{ route('prepaid_post') }}">
    @csrf
    @if($errors->has('message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <strong>Failed!</strong> {{ $errors->first('message') }}.
        </div>
    @endif
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus placeholder="Mobile Number">
            @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <select id="value_prepaid" class="form-control @error('value_prepaid') is-invalid @enderror" name="value_prepaid" required>
                <option hidden>Value</option>
                <option value="10000" {{ substr(old('value_prepaid'), 0, -1) == '10000' ? 'selected' : '' }}>10.000</option>
                <option value="50000" {{ substr(old('value_prepaid'), 0, -1) == '50000' ? 'selected' : '' }}>50.000</option>
                <option value="100000" {{ substr(old('value_prepaid'), 0, -1) == '100000' ? 'selected' : '' }}>100.000</option>
            </select>
            @error('value_prepaid')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center fixed-bottom">
        <div class="col-md-6 col-sm-11">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
        </div>
    </div>
</form>
@endsection
