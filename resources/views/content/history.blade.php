@extends('layouts.app')
@section('content')
<h2 for="email" class="col-md-4 text-md-right">{{ __('Pay your order') }}</h2>
<form method="POST" action="{{ route('payment_post') }}">
    @csrf
    @if($errors->has('message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <strong>Failed!</strong> {{ $errors->first('message') }}.
        </div>
    @endif
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-12 col-sm-12">
            <ul class="list-group list-group-flush">
                @foreach($order_list as $ol)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-10">
                            <div class="fw-bold">
                                <div class="d-flex justify-content-between">
                                    <div><h5 >{{ $ol->po_order_code }}</h5></div>
                                    <div><h5 >Rp {{ number_format($ol->po_total, 0,'.','.' )  }}</h5></div>
                                 </div>
                            </div>
                            @if($ol->p_type == 1)
                            {{ $ol->p_value }} for {{ $ol->p_mobile_phone }} 
                            @else
                            {{ $ol->p_name }} that costs {{ $ol->p_price }} 
                            @endif
                        </div>
                        <div class="col-2">
                            @if($ol->po_status == 1)
                            <p class="text-success">Success</p> 
                            @elseif($ol->po_status == 2)
                            <p class="text-warning">Failed</p>
                            @elseif($ol->po_status == 3)
                            <a href="{{route('payment_selected', encrypt($ol->po_id))}}" type="button" class="btn btn-sm btn-primary">Pay Now</a>
                            @elseif($ol->po_status == 4)
                            <p class="text-danger">Cancel</p>
                            @else
                            <p class="text-body">Shipping code <br>{{ $ol->p_shipping_code }}</p>
                            @endif
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        {{ $order_list->links() }}
    </div>
</form>
@endsection
