@extends('layouts.app')
@section('content')
<h2 for="email" class="col-md-4 text-md-right">{{ __('Product Page') }}</h2>
<form method="POST" action="{{ route('product_post') }}">
    @csrf
    @if($errors->has('message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <strong>Failed!</strong> {{ $errors->first('message') }}.
        </div>
    @endif
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <textarea id="Product" class="form-control @error('product') is-invalid @enderror" name="product" required autocomplete="product" autofocus placeholder="Product" rows="4">{{ old('product') }}</textarea>
            @error('product')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <textarea id="shipping_address" class="form-control @error('shipping_address') is-invalid @enderror" name="shipping_address" required autocomplete="shipping_address" autofocus placeholder="Shipping Address" rows="4">{{ old('shipping_address') }}</textarea>
            @error('shipping_address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" required autocomplete="price" autofocus placeholder="Price">
            @error('price')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center fixed-bottom">
        <div class="col-md-6 col-sm-11">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
        </div>
    </div>
</form>
@endsection
