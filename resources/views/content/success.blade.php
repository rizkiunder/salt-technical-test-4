@extends('layouts.app')
@section('content')
<h2 class="col-md-4 text-md-right"><b>{{ __('Success!') }}</b></h2>
<form method="POST" action="{{ route('success_post') }}">
    @csrf
    @if($errors->has('message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <strong>Failed!</strong> {{ $errors->first('message') }}.
        </div>
    @endif
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-11 col-sm-11 col-offset-md-1">
            <div class="d-flex justify-content-between">
                <div><h5 >{{ __('Order no.') }}</h5></div>
                <div><h5 >{{ $order_id }}</h5></div>
             </div>
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-11 col-sm-11 col-offset-md-1">
            <div class="d-flex justify-content-between">
                <div><h5 >{{ __('Total.') }}</h5></div>
                <div><h5 >Rp {{ $total }}</h5></div>
             </div>
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-11 col-sm-11 col-offset-md-1">
            <div>{!! $message !!}</div>
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center fixed-bottom">
        <div class="col-md-6 col-sm-11">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
        </div>
    </div>
</form>
@endsection
