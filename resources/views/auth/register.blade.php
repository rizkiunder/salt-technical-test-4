@extends('layouts.app')
@section('content')
<h2 for="email" class="col-md-4 text-md-right">{{ __('Login') }}</h2>
<form method="POST" action="{{ route('register') }}">
    @csrf
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Register') }}</button>
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11 text-center">
            <a href="#" class="text-primary"><b>{{ __('Login') }}</b></a>
        </div>
    </div>
</form>
@endsection
