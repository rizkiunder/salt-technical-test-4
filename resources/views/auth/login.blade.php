@extends('layouts.app')
@section('content')
<h2 for="email" class="col-md-4 text-md-right">{{ __('Login') }}</h2>
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Login') }}</button>
        </div>
    </div>
    <div class="form-group row d-flex justify-content-center">
        <div class="col-md-6 col-sm-11 text-center">
            <a href="{{ route('register') }}" class="text-primary"><b>{{ __('Register') }}</b></a>
        </div>
    </div>
</form>
@endsection
