<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\ProductOrder;
class orderStatusUnpaid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $count_unpaid = Product::join('product_order', 'p_id', 'po_p_id')
                                 ->where([
                                            ["p_user", Auth::user()->id]
                                        ])
                                 ->where('po_status', 3)
                                 ->count('p_id');
        session(['unpaid'=>$count_unpaid]);
        return $next($request);
    }
}
