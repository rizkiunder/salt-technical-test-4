<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use Redirect;
use App\Models\Product;
use App\Models\ProductOrder;
use App\Jobs\OrderJob;
class AfterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'order_count']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function success()
    {
        $product_detail = Product::join('product_order', 'p_id', 'po_p_id')
                                 ->where([
                                            ["p_user", Auth::user()->id]
                                        ])
                                 ->orderBy('po_timestamp', 'DESC')
                                 ->first();
        $value = number_format($product_detail->po_total, 0, ',', '.');
        $order_id = (string)$product_detail->po_order_code;
        $order_id = str_split($order_id, strlen($order_id)/5);
        if($product_detail->p_type == 1){
            $message = 'Your mobile phone number '.$product_detail->p_phone_number.' will receive Rp '.$value; 
        }else if($product_detail->p_type == 2){
            $message = $product_detail->p_name.' that costs '.$product_detail->p_price.' will bee shipped to: <br/> '.$product_detail->p_address.' <br/> only after you pay.';
        }
        $data = array(
            'order_id' => $order_id[0].$order_id[1].' '.$order_id[2].$order_id[3].' '.$order_id[4],
            'total' => $value,
            'message' => $message,
        );
        return view('content.success')->with($data);
    }

    public function payment()
    {
        $product_detail = Product::join('product_order', 'p_id', 'po_p_id')
                                 ->where([
                                            ["p_user", Auth::user()->id]
                                        ])
                                 ->orderBy('po_timestamp', 'desc')
                                  ->first();
        $value = number_format($product_detail->po_total, 0, ',', '.');
        $order_id = (string)$product_detail->po_order_code;
        $order_id = str_split($order_id, strlen($order_id)/5);
        if($product_detail->p_type == 1){
            $message = 'Your mobile phone number '.$product_detail->p_phone_number.' will receive Rp '.$value; 
        }else if($product_detail->p_type == 2){
            $message = $product_detail->p_name.' that costs '.$product_detail->p_price.' will bee shipped to: <br/> '.$product_detail->p_address.' <br/> only after you pay.';
        }
        $data = array(
            'order_id' => $product_detail->po_order_code,
            'total' => $value,
            'message' => $message,
        );
        return view('content.payment')->with($data);
    }

    public function payment_selected($id)
    {
        $product_detail = Product::join('product_order', 'p_id', 'po_p_id')
                                 ->where([
                                            ["p_user", Auth::user()->id],
                                            ['po_id', decrypt($id)]
                                        ])
                                 ->orderBy('po_timestamp', 'desc')
                                  ->first();
        $value = number_format($product_detail->po_total, 0, ',', '.');
        $order_id = (string)$product_detail->po_order_code;
        $order_id = str_split($order_id, strlen($order_id)/5);
        if($product_detail->p_type == 1){
            $message = 'Your mobile phone number '.$product_detail->p_phone_number.' will receive Rp '.$value; 
        }else if($product_detail->p_type == 2){
            $message = $product_detail->p_name.' that costs '.$product_detail->p_price.' will bee shipped to: <br/> '.$product_detail->p_address.' <br/> only after you pay.';
        }
        $data = array(
            'order_id' => $product_detail->po_order_code,
            'total' => $value,
            'message' => $message,
        );
        return view('content.payment')->with($data);
    }

    public function history()
    {

        OrderJob::dispatch();
        $product_detail = ProductOrder::join('product', 'po_p_id', 'p_id')
                                 ->where([
                                            ["p_user", Auth::user()->id]
                                        ])
                                 ->orderBy('po_timestamp', 'DESC')
                                 ->paginate(2);
        foreach ($product_detail as $key => $pd) {
            $order_id = (string)$pd->po_order_code;
            $order_id = str_split($order_id, 4);
            $pd->po_order_code = $order_id[0].' '.$order_id[1].' '.$order_id[2];
        }
        $data = array(
            'order_list' => $product_detail
        );
        return view('content.history')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_success(Request $request)
    {
        return Redirect::route('payment');
    }

    public function store_payment(Request $request)
    {
        $product_detail = Product::join('product_order', 'p_id', 'po_p_id')
                                 ->where([
                                            ["p_user", Auth::user()->id],
                                            ["po_order_code", $request->post('order_id')],
                                        ])
                                  ->first();
        if($product_detail->p_type == 2){
            Product::join('product_order', 'p_id', 'po_p_id')
                    ->where([
                              ["p_id", $product_detail->p_id]
                          ])
                    ->update(
                                [
                                    'p_shipping_code' => substr(md5(uniqid(mt_rand(), true)), 0, 8),
                                    'po_status' => 5,
                                ]
                            );
        }else if($product_detail->p_type == 1){
            $arr = array(1, 0);
            $rand = (float)rand()/(float)getrandmax();
            $time_now = date('H:i:s');
            if($time_now >= '09:00:00' && $time_now <= '17:00:00'){
                $success_rate = 0.9;
            }else{
                $success_rate = 0.4;
            }
            $fail_rate = 1 - $success_rate;
            if($rand > $fail_rate){
                $state = 1;
            }else{
                $state = 2;
            }
            Product::join('product_order', 'p_id', 'po_p_id')
                    ->where([
                              ["p_id", $product_detail->p_id]
                          ])
                    ->update(
                                [
                                    'po_status' => $state,
                                ]
                            );

        }
        return Redirect::route('history');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
