<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use Redirect;
use App\Models\Product;
use App\Models\ProductOrder;
class ProductController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth', 'order_count']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('content.product_page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product' => 'required|min:10|max:150',
            'shipping_address' => 'required|min:10|max:150',
            'price' => 'required|numeric',
        ], $message = [
                'product.required' => 'Phone nunber is required',
                'product.min' => 'Minimum 10 character',
                'product.max' => 'Minimum 150 character',
                'shipping_address.required' => 'Phone nunber is required',
                'shipping_address.min' => 'Minimum 10 character',
                'shipping_address.max' => 'Minimum 150 character',
                'price.required' => 'Value is required',
                'price.numeric' => 'Only numbers'
            ]);
        if($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withMessage($message)->withInput();
        }
        DB::beginTransaction();
        try{
            $product_id = Product::insertGetId(
                                [
                                    'p_name' => $request->post('product'), 
                                    'p_address' => $request->post('shipping_address'),
                                    'p_price' => $request->post('price'),
                                    'p_user' => Auth::user()->id,
                                    'p_type' => 2,
                                ]
                            );
            ProductOrder::insert(
                                [
                                    'po_p_id' => $product_id, 
                                    'po_order_code'=> rand(1111111111,9999999999),
                                    'po_total' => $request->post('price') + 10000,
                                    'po_status' => 3
                                ]
                            );
            DB::Commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            return Redirect::back()->withErrors(["message" => "Went something wrong, Please try again later"])->withInput();
        }
        return Redirect::route('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
