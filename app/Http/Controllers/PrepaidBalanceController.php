<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use Redirect;
use App\Models\Product;
use App\Models\ProductOrder;
class PrepaidBalanceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'order_count']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('content.prepaid_balance');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'value_prepaid' => $request->post('value_prepaid').'>',
        ]);
        $validator = Validator::make($request->all(), [
            'phone' => 'required|digits_between:7,12|regex:/(081)/',
            'value_prepaid' => 'required|in:10000>,50000>,100000>',
        ], $message = [
                'phone.required' => 'Phone nunber is required',
                'phone.digits_between' => 'Phone number must be 7 to 12 digits',
                'phone.regex' => 'The prefix must be 081',
                'value_prepaid.required' => 'Value is required',
                'value_prepaid.in' => 'Permissible values ​​are 10,000, 50,000, 100,000',
            ]);
        if($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withMessage($message)->withInput();
        }
        DB::beginTransaction();
        try{
            $product_id = Product::insertGetId(
                                [
                                    'p_mobile_phone' => $request->post('phone'), 
                                    'p_value' => substr($request->post('value_prepaid'), 0, -1),
                                    'p_user' => Auth::user()->id,
                                    'p_type' => 1,
                            ]);
            ProductOrder::insert(
                                [
                                    'po_p_id' => $product_id, 
                                    'po_order_code'=> rand(1111111111,9999999999),
                                    'po_total' => intval($request->post('value_prepaid'))+(intval($request->post('value_prepaid'))*5/100),
                                    'po_status' => 3,
                                ]
                            );
            DB::Commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return Redirect::back()->withErrors(["message" => "Went something wrong, Please try again later"])->withInput();
        }
        return Redirect::route('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
