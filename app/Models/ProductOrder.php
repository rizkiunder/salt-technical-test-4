<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    use HasFactory;
    protected $table = 'product_order';
    protected $fillable = [
    						'po_p_id',
    						'po_order_code',
    						'po_total',
    						'po_status',
    						'po_timestamp'
    					];
    protected $primaryKey = 'po_id';
    public $timestamps = false;
}
