<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'product';
    protected $fillable = [
    						'p_name',
    						'p_address', 
    						'p_price',
    						'p_mobile_phone',
    						'p_value',
    						'p_state',
    						'p_type',
    						'p_user',
    						'p_timestamp',
    					];
    protected $primaryKey = 'p_id';
    public $timestamps = false;
}
