<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrepaidBalance extends Model
{
    use HasFactory;
    protected $table = 'prepaid_balance';
    protected $fillable = ['pb_phone','pb_value'];
    protected $primaryKey = 'pb_id';
    public $timestamps = false;
}
