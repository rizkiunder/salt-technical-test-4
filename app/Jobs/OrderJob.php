<?php

namespace App\Jobs;

use Closure;
use Session;
use Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\ProductOrder;
class OrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $list_unpaid = Product::join('product_order', 'p_id', 'po_p_id')
                               ->where([
                                         ["po_timestamp",'<', strtotime('-5 minutes')],
                                         ["po_status", 3]
                                     ])
                               ->update(
                                           [
                                               'po_status' => 4,
                                           ]
                                       );
    }
}
