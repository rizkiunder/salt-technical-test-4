<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/prepaid-balance', [App\Http\Controllers\PrepaidBalanceController::class, 'index'])->name('home');
Route::post('/prepaid-balance', [App\Http\Controllers\PrepaidBalanceController::class, 'store'])->name('prepaid_post');
Route::get('/product', [App\Http\Controllers\ProductController::class, 'index'])->name('product');
Route::post('/product', [App\Http\Controllers\ProductController::class, 'store'])->name('product_post');
Route::get('/success', [App\Http\Controllers\AfterController::class, 'success'])->name('success');
Route::post('/success', [App\Http\Controllers\AfterController::class, 'store_success'])->name('success_post');
Route::get('/payment', [App\Http\Controllers\AfterController::class, 'payment'])->name('payment');
Route::post('/payment', [App\Http\Controllers\AfterController::class, 'store_payment'])->name('payment_post');
Route::get('/payment/{id}', [App\Http\Controllers\AfterController::class, 'payment_selected'])->name('payment_selected');
Route::get('/order', [App\Http\Controllers\AfterController::class, 'history'])->name('history');
